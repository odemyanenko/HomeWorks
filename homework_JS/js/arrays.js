/**
Working with arrays in Javascript
You may search the array to find one item via some method.
Array.some

You may process all array items via forEach method.
Array.forEach

You may join two arrays via concat method.
Array.concat
arr1.concat(arr2) - will add items of arr2 to the arr1.

Adds a new item to the end of the array.
Array.push

Removes the first element from the array.
Array.shift

Adds a new item to the beginning of the array.
Array.unshift

Remove the last element from the array.
Array.pop

Method splice may be used to remove item by its index in the array.
Array.splice
arr.splice(2, 1) - will remove third item in the array.

Methods like slice will return new array. Calling slice without any argument will return new copy of the array.
Array.slice
arr.slice() - will return clone of the array arr.
 */

//Capitalised Names
/*
When secretary added new Users to our list of students, capslock was broken, so now all names start with a 
lowercase first letter; You need to write a function and return new array with all names inside it 
starting with the first symbol in an upperCase.
*/

function makeFirstSymbolUpperCase(arrayList) {
  for (let i = 0; i < arrayList.length; i++) {
    arrayList[i] = arrayList[i].toLowerCase();
    arrayList[i] = arrayList[i].replace(/\b\w/g, varLetter => varLetter.toUpperCase());
  }
  return arrayList;
};
/*
let res;
res = makeFirstSymbolUpperCase (["aleN mark", "koel macdoyl", "ivan petroV"]);
console.log(res);

res = makeFirstSymbolUpperCase(['stacy','lora','volodymyr']);
console.log(res);
*/

/**
 Sum of arguments
There is a sum function that should return the sum of all the arguments given to it, but there is a problem 
that the number of arguments can be from 2 to 20, we don't know exactly how many arguments will be.

Restriction: You need to write code in a functional way, without for-loops

Array Object
Array.prototype.reduce
Arguments Object
 */

function sum() {
  let args = [].slice.call(arguments);
  return args.reduce(function (accumulator, currentValue) {
    return accumulator + currentValue;
  })
}
/*
let res;
res = sum(10, 20, 30);
alert(res);
res = sum(10, 20);
alert(res);
*/

//Find an object in the array
/*
Given an array of objects containing field id, write a function that will receive as arguments such array and an id value. 
Using modern Array methods, this function should find one element whose id is identical to the provided one as the argument 
and return the found object.
 */

function findUser(arr, id) {
  let found = arr.find(function (element) {
    return (element["id"] === id);
  });
  return found;
}
/*
let res;
res = findUser([{ "id": 1, "name": "element1" }, { "id": 2, "name": "element1" }, { "id": 3, "name": "element1" }], 3);
console.log(res);
res = findUser([{ "id": 1, "name": "element1" }, { "id": 2, "name": "element1" }, { "id": 3, "name": "element1" }], 1);
console.log(res);*/

//Combine two arrays in one
/*Write a function that will receive two arrays as arguments and return the combined array of provided two.
For example, combineArrays([1, 2, 3], [4, 5]) should return [1, 2, 3, 4, 5];
 */
function combineArrays(arrA, arrB) {
  return arrA.concat(arrB);
}

/*
let res;
res = combineArrays([1, 2, 3], [4, 5]);
alert(res);
*/

//Data Stack
/*Given an array that represents a stack of some data with the limited amount of data in it, 
write a function that will add to the end of the array new value and at the same time will 
remove the first item in an array, so that total length of the array will remain the same.
For example, addToStack([1, 2, 3], 4) should return [2, 3, 4];*/

function addToStack(arr, item) {
  arr.shift();
  arr.push(item);
  return arr;
}
/*
let res;
res = addToStack([1, 2, 3], 4);
alert(res);
*/
// Remove the first item in the array ////////LAST FIRST!!!!!!
/*Write a function that will remove the first item in the provided array and 
return modified array.
For example, removeLast([1, 2, 3]) should return [1, 2].
 */

function removeLast(arr) {
  //return arr.splice(0, arr.length - 1);
  arr.pop();
  return arr;
}
/*
let res;
res = removeLast([1, 2, 3]) ;
alert(res);*/

// Remove by array Index
/*Write a function that will receive two argument - an array and an index. 
Remove array element at the index position (index starts from 0) and return modified array. 
Array item must be removed, not set to undefined, 0 or null. Array length should change.
For example, removeByIndex([1, 2, 3, 4], 2) should return [1, 2, 4].
 */
function removeByIndex(arr, index) {
  arr.splice(index, 1);
  return arr;
}
/*
let res;
res = removeByIndex([1, 2, 3, 4], 2);
alert(res);
*/

//Get the largest number in the array
/*
Write a function that will receive an array of numbers (including negative numbers and/or zero) 
and will return the largest number in it.
For example, greatestValue([1, -7, -2, 10, 4]) will return 10.
 */
function greatestValue(arr) {
  let maxValue = arr[0];
  for (i = 1; i < arr.length; i++) {
    if (arr[i] > maxValue) {
      maxValue = arr[i];
    }
  }
  return maxValue;
}
/*
let res;
res = greatestValue([1, -7, -2, 10, 4]);
alert(res);
*/

/**
 Add array values
Write a simple function that will return a sum of numbers in the provided array.

For example, sumValue([1, 2, 3]) will return 6.

Array object
 */

function sumValue(arr) {
  let sumResult = 0;
  arr.forEach(item => { sumResult += item; });
  return sumResult;
}
/*
let res;
res = sumValue([1, 2, 3]);
alert(res);
*/

//Find near in array
/*Write a function that will receive an array of numbers and a number. Return true if the array 
contains the number with +/- 1 accuracy. I.e. 3 will be true for range from 2 to 4.
For example, containsNear([1, 2, 3], 4) will return true.
 */

function containsNear(arr, val) {
  let isFound = arr.some(function (element) {
    return (element >= val - 1 && element <= val + 1);
  });
  return isFound;
}

let res;
res = containsNear([1, 2, 3], 4);
alert(res);
res = containsNear([1, 2, 3], 7);
alert(res);
res = containsNear([1, 2, 3], 2);
alert(res);
res = containsNear([10, 21, 12], 21);
alert(res);

