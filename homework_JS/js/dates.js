
/*
Working with Data object examples.

Date object
var date = new Date() - creation of new Date object.
var date = new Date("3/20/2018") - will create a Date object with specific date (Various date formats are supported).
date.getDay() - will return day of the week (a number from 0 to 6).
date.getDate() - will return day of the month (a number from 1 to 31).
date.getMonth() - will return month (a number from 0 to 11).
date.getFullYear() - will return the year.
 */

//Get a number of days
/*Write a function that will receive two arguments - year and month (1-12). 
This function should return the number of days in the selected month of the selected year.
For example, getNumberOfdays(2018, 2) will return a number 28.*/
function getNumberOfdays(year, month) {
  let lastDateOfMonth = new Date(year, month, 0).getDate();
  return lastDateOfMonth;
}

/*
let res;
res = getNumberOfdays(2018, 2);
alert(res);
res = getNumberOfdays(2018, 7);
alert(res);*/


//Show time
/**
 * Write a function that will receive as argument one number which is a Unix timestamp. 
 * Based on this timestamp function should return a string value in the format of HH:MM:SS 
 * of time set in this timestamp. If an hour, minute or second is less than 10 - add null 
 * before it (so that 2 displayed as 02).
 * Example of such string - 05:04:01.
 */

function showTime(timestamp) {
  let date = new Date(timestamp);// * 1000);

  console.log(date.toLocaleDateString);

  let hours = "0" + date.getHours();
  let minutes = "0" + date.getMinutes();
  let seconds = "0" + date.getSeconds();

  return (hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2));
}

let res;
//res = showTime(Math.floor(Date.now() / 1000));
///res = showTime(1532675196);
//alert(res);

//res = showTime(1532528595);//# Wed Jul 25 16:23:15 2018
//alert(res);

res = showTime(1000001041000);
alert(res);

//console.log((new Date()).toLocaleTimeString());
//# ‎10‎:‎14‎:‎04


//Current date
/**
Write a function that will return current date in an object with the fields day, month and year.
For example, on 1 January 2018 getCurrentDate() should return {day: 1, month: 1, year: 2018}.
 */

function getCurrentDate() {
  let dateObj = new Object();
  let currDate = new Date();

  dateObj.day = currDate.getDate();
  dateObj.month = currDate.getMonth() + 1;
  dateObj.year = currDate.getFullYear();

  return dateObj;
}
/*
let res = getCurrentDate();
console.log(res);
*/

//Get the month name
/*Given an array of month names - write a function that will receive date object and based on it will return it's current month name.
Array of month names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"].

For example, getMonthName(new Date("10/25/2018")) must return October.
* */

function getMonthName(date) {
  let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  return monthNames[date.getMonth()];
}
/*
let res = getMonthName(new Date("10/25/2018"));
alert(res);*/


//Weekend
/* Write a function that will receive date object and will return true if the provided date is weekend and false if not.
For example, isWeekend(new Date("1/20/2018") must return true.*/

function isWeekend(date) {
  return (date.getDay() === 0 || date.getDay() === 6);
}

/*
let res;
res = isWeekend(new Date("1/20/2018"));
alert(res);
res = isWeekend(new Date("7/29/2018"));
alert(res);
res = isWeekend(new Date("7/27/2018"));
alert(res);*/

/**
 * Days passed

Write a function that will receive as arguments two objects - date1 and date2. Calculate how many days 
have passed from date1 to date2.

For example, daysPassed(new Date("2/20/2018"), new Date("11/1/2018")) will return 254.

Date object

 */

function daysPassed(date1, date2) {
  let oneday_ms = 1000 * 60 * 60 * 24;
  let timediff_ms = Math.abs(date2.getTime() - date1.getTime());

  return Math.ceil(timediff_ms / oneday_ms);
}

/*
let res;
res = daysPassed(new Date("2/20/2018"), new Date("11/1/2018"));
alert(res);
res = daysPassed(new Date("07/27/2018"), new Date("07/30/2018"));
alert(res);
*/

//Get last day
/*
Write a function that will receive a year and a month number (starting from 1). Return last day of the provided month.
For example, lastDay(2018, 2) will return 28.*/
function lastDay(year, month) {
  let lastDateOfMonth = new Date(year, month, 0);
  return lastDateOfMonth.getDate();
}
/*
let res;
res = lastDay(2018, 2);
alert(res);
res = lastDay(2018, 7);
alert(res);*/


//Days in a year
/*Write a function that will return the number of days in the provided as argument year.
For example, daysInYear(2018) will return 365.*/

function daysInYear(year) {
  let firstDateOfYear = new Date(year, 0, 1);
  let firstDateOfNextYear = new Date(year + 1, 0, 1);

  let oneday_ms = 1000 * 60 * 60 * 24;

  let timediff_ms = firstDateOfNextYear.getTime() - firstDateOfYear.getTime();

  return Math.floor(timediff_ms / oneday_ms);
}

/*
let res;
res = daysInYear(2018);
alert(res);
res = daysInYear(2016);
alert(res);
*/


//Days of the year passed
/*Write a function that will receive a date object and will return how many full days have passed since the beginning of the current year.
For example, fullDaysPassed( new Date("1/20/2018")) must return 19.*/

function fullDaysPassed(date) {
  let oneday_ms = 1000 * 60 * 60 * 24;

  let firstDateOfYear = new Date(date.getFullYear(), 0, 1);
  let timediff_ms = date.getTime() - firstDateOfYear.getTime();
  
  return Math.floor(timediff_ms / oneday_ms);
}
/*
let res;
res = fullDaysPassed( new Date("1/20/2018"));
alert(res);
res = fullDaysPassed( new Date(2018, 0, 2, 20, 44, 44));
alert(res);*/

//Get year in a short form
/*Write a function that will receive date object and will return two last digits of a provided year.
For example, getYearShort(new Date("1/20/2017")) must return 17.
 */

function getYearShort(date) {
  let year = date.getFullYear(date);  
  return (year % 100);
}
/*
let res;
res = getYearShort(new Date("1/20/2017"));
alert(res);
res = getYearShort(new Date("8/20/1915"));
alert(res);
res = getYearShort(new Date);
alert(res);
*/