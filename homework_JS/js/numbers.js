/**
 Working with numbers in Javascript
Number Object
Math Object

To convert string to a number you may use:
parseInt
parseFloat

parseInt("1.22") - will parse string to integer and will return number 1.
parseInt("1.22") - will parse string to float and will return number 1.22.

You may round number using one of the following methods
Math.round(1.5) - will return to nearest integer - 2.
Math.ceil(1.5) - will return to biggest integer - 2.
Math.floor(1.5) - will return to lowest integer - 1.

Math.random() May be used to generate random number from 0 to 1.
Math.floor((Math.random() * 10) + 1) - will return random number from 1 to 10.

parseInt May be used as well to parse the string from a specific numeric system.
parseInt("10101", 2) - will parse string 10101 as binary number and will return 21.

toString Method of Number object can accept an argument to show the number in the different numeric system.
(21).toString(2) - will return string 10101.
 */

//FizzBuzz
/*
You need to write a function that will return an array of numbers from 1 to 100. 
But, if a number divides without remnant on 3 - then replace it with the string Fizz. 
If a number divides without remnant on 5 - replace it with the string Buzz. 
If a number divides without remnant both on 3 and 5 - replace it with the string FizzBuzz.

Example of such array: [1, 2, 'Fizz', 4, 'Buzz', .... , 13, 14, 'FizzBuzz', ...]
 */

function fizzBuzz () {
  let resultArray = [];
  for (let i = 1; i <= 100; i++){
    if (i % 3 === 0 && i % 5 === 0){
      resultArray.push('FizzBuzz');
    }
    else if (i % 5 === 0) {
      resultArray.push('Buzz');      
    }
    else if (i % 3 === 0) {
      resultArray.push('Fizz');      
    }
    else resultArray.push(i);      
  }  
  return resultArray;
}
/*
alert(fizzBuzz());
*/

//Math with dynamic-typed values
/*
Given the function that receives two arguments - number A and number B, add them and return the result. 
However, number A and number B can be a String representing a number - in that case, you need to cast them to a numeric value before you will add them. 
If one or both of provided values are not a Number and not a String that can be cast to a number - return false instead.
For example, addNumbers("2", 2) will return a number 4.
 */

function addNumbers(numA, numB) {  
  numA = Number(numA);
  numB = Number(numB);
  if (!isNaN(numA) && !isNaN(numB)) {
    return numA + numB;
  }
  else return false;
}
/*
let res;
res = addNumbers("2", 2);
alert(res);
res = addNumbers("45", 2.34);
alert(res);
res = addNumbers("45sdfs", 2.34);
alert(res);
*/

//Random dice
/*Create a function that will return a random integer between 1 and 6. 
Use Math.random to generate a random number.
For example, randomDice() may return 3 (but number needs to be 
random every time, not preset).
 */
function randomDice() {   
   return Math.floor(Math.random() * 5 + 1); 
}
/*
let res;
res = randomDice();
alert(res);
res = randomDice();
alert(res);
res = randomDice();
alert(res);*/

//Binary to decimal
/*Write a function that will receive a binary number as a string (like "111000111") and 
will return its decimal equivalent as an integer.
For example, bin2Dec("11110") will return number 30.*/

function bin2Dec(bin) {
  return parseInt(bin, 2);
}
/*
let res;
res = bin2Dec("11110");
alert(res);
*/

//Decimal to binary
/*Write a function that will receive a decimal number and will return its 
binary equivalent as a string (like "11011").
For example, dec2Bin(123) will return string 1111011.*/

function dec2Bin(dec) {
  return dec.toString(2);
}
/*
let res;
res = dec2Bin(123);
alert(res);
*/

//Natural number
/*Check that provided number is a natural, ie numbers like 0, 1, 2, 3... 
but not 0.42, 1.2, 10.5, etc. Return boolean value.
For example, isNatural(123) will return boolean true.*/

function isNatural(n) {
  return Number.isInteger(n);
}
/*
let res;
res = isNatural(123);
alert(res);
res = isNatural(0.42);
alert(res);
*/

//Sum of values in an array
/*Given an array of mixed values, write a function that will accept such array as an argument 
and will return a sum of all numeric values in the array. If a string contains number - 
it must be cast to it and calculated as well. Non-number values and string that can't 
be cast to a number must be ignored.

For example, sumNumbers([1, "2", "abc", 3]) must return number 6.*/

function sumNumbers(arr){
  let sumResult = 0;
  arr.forEach(element => {
    if (!isNaN(Number(element))) {
      sumResult += Number(element);
    }
  });
  return sumResult;
}
/*
let res;
res = sumNumbers([1, "2", "abc", 3]);
alert(res);
*/

//Round a number
/*Write a function that will round a provided number to a specified number of 
a provided digits. So a number 123.123123 with param 2 must return 123.12. 
Return a number. Use parseFloat and toFixed methods.

For example, roundNumber(123.59, 1) will return a number 123.5.*/
function roundNumber(number, digits) {
  return  +Number.parseFloat(number).toFixed(digits);
}

let res;
res = roundNumber(123.123123, 2);
alert(res);
res = roundNumber(123.59, 1);
alert(res);