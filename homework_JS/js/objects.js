//Palindrome
/*Given the string, check if it's a palindrome.
Return true if it is a palindrome, otherwise, return false.
For example, checkPalindrome("aabaa") will return boolean true.*/
function checkPalindrome(inputString) {
  let valStr = String(inputString).replace(/\s+/g, "");

  // Step 1. Use the split() method to return a new array
  let splitString = valStr.split(""); // var splitString = "hello".split("");
  // ["h", "e", "l", "l", "o"]

  // Step 2. Use the reverse() method to reverse the new created array
  let reverseArray = splitString.reverse(); // var reverseArray = ["h", "e", "l", "l", "o"].reverse();
  // ["o", "l", "l", "e", "h"]

  // Step 3. Use the join() method to join all elements of the array into a string
  let reverseStr = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
  // "olleh"

  //Step 4. Return the reversed string
  return (reverseStr === valStr); // "olleh"

}
/*
alert(checkPalindrome("aabaa"));
alert(checkPalindrome("vsdvs ssdfsd"));
alert(checkPalindrome("а роза упала на лапу азора"));
*/

//Working with string characters
/*Given the string, character A and character B. Replace in the string all characters A to characters B (case-sensitive).*/

function replaceLetter(str, charA, charB) {
  str = str.split(charA.charAt(0)).join(charB.charAt(0));
  return str;
}

//alert(replaceLetter("sfdsfsd sdf sdf sdf ", "s", "a"));

//Object creation
/*You need to create an object using function-constructor.
The object must have string fields model and type.
The object must have boolean fields isActive, its default value must be false.

example of the constructed object:
{
type: "car",
model: "bmv",
isActive: false
} */

function Vehicle() {
  this.model = "";
  this.type = "";
  this.isActive = false;
};
/*
let obj = new Vehicle();
obj.model = "bmv";
obj.type = "car";

console.log(obj);*/

//Address Book
/*We already have constructor AddressBook and few instances of it, but we want to improve it, 
so every instance of it can have method checkPhoneLength that compares the length of phone 
property is bigger than 5. Extend the constructor with a new method using chain inheritance.*/

function AddressBook(firstName, phone) {
  this.firstName = firstName;
  this.phone = phone;

  return this;
}

/*
let stacy = new AddressBook ('Stacy', '3217');
let alex =  new AddressBook('Alex','0441234455');

AddressBook.prototype.checkPhoneLength = function(){
  return (this.phone.length > 5) ;
}

console.log(stacy.checkPhoneLength());
console.log(alex.checkPhoneLength());
*/

//User Profile
/*Please write constructor named 'UserProfile' that will create an object 
with same properties as in object below.

var john = {
firstName: "John",
lastName: "Smith",
phoneNumber: "0567772244"
};

Also, the constructor must have a method named 'getPersonalData' which must return a 
string with the first and last name John Smith as an example.
 */

function UserProfile(firstName, lastName, phoneNumber) {
  let obj = {
    'firstName': firstName,
    'lastName': lastName,
    'phoneNumber': phoneNumber,
    getPersonalData: function () {
      return this.firstName + ' ' + this.lastName;
    }
  }
  return obj;
}

/*
let john = UserProfile("John", "Smith", "0567772244");
console.log(john);
console.log(john.getPersonalData());
*/

//Extending Prototypes
/*Given a numeric array, extend Array prototype with a new method called double. 
This method should multiply all numbers in the array by 2. 
This method should modify original array.

Object Prototype
Working with Prototypes
*/
//extend Array object

if (!Array.prototype.double) {
  Array.prototype.double = function () {
    for (let i = 0; i < this.length; i++) {
      this[i] *= 2;
    };
    return this;
  }
}

/*
let arr = new Array;
arr = [1,2,3,4,5,6];
arr.double();
console.log(arr);*/

//Assigning Prototypes
/*Given object Car and object constructor BMV, make so that constructor BMV will 
return new objects whose prototype will be object Car.
Object Prototype
 */

let Car = {
  isMachine: true
}

function BMV(model) {
  function F() { this.model = model };
  F.prototype = Car;
  let f = new F;
  return f;
}
/*
let obj = new BMV('X5');
console.log(obj);
console.log(obj.isMachine);
*/

//Delete property
/*Write a function that will receive an object and a name of the property as a string. 
Delete the selected property from the object and return it.
For exmaple, having object obj = {a: 1, b: 2, c: 3} calling 
deleteProperty(obj, "b") will return {a: 1, c: 3}.
 */


function deleteProperty(obj, field) {
  delete obj[field];
  return obj;
}
/*
let obj = { a: 1, b: 2, c: 3 };
console.log(deleteProperty(obj, "b"));*/
//alert(deleteProperty(obj, "b"));// will return {a: 1, c: 3}.


//Call method from the prototype
/*Write object constructor Constructor with method callFromPrototype that will 
receive a name of the method as a string. On calling this method on the constructed 
object - call the method of the constructors prorotype. Return value that is 
returned from the prototype method. A prototype will be assigned to the constructor during testing.

For example,

var obj = new Constructor;
obj.callFromPrototype("helloWorld")
will call method helloWorld from prototype of the Constructor and will 
return any value returned by the helloWorld
 */
function Constructor() {
  this.callFromPrototype = function (property) {
    //write your code here
    //return this[property].apply(this); 
    //return this.prototype[property].call(this);
    //return this[property](this); 
    return Object.prototype.hasOwnProperty.call(this, property);
  };
}
/*
var obj = new Constructor;
alert(obj.callFromPrototype("helloWorld"));
*/

//Get object keys
/*Write a function that will return an array of the provided object keys.
For example, getKeys( {name: "Dan", age: 22, department: "IT"} ) will 
return an array ["name", "age", "department"].*/
function getKeys(obj) {
  return Object.keys(obj);
}

//alert(getKeys({ name: "Dan", age: 22, department: "IT" }));//will return an array ["name", "age", "department"]

//Get object constructor name
/*Write a function that will receive an object. Return the name of this 
object constructor. So for a number value, it will be Number, for a string value - String. 
If it is a user-created object - return the name of its constructor-function.

For example, showConstructor(userObject) may return UserConstructor, if an object userObject was created via new UserConstructor.
 */
function showConstructor(obj) {
  var funcNameRegex = /function (.{1,})\(/;
  var results = (funcNameRegex).exec((obj).constructor.toString());
  return (results && results.length > 1) ? results[1] : "";
}

//Copy properties
/*Create a function that will receive links to two objects as arguments. Copy properties 
from the second object to the first one. Make a simple copy, no recursion required.

For example, given objA = {a: 1} and objB = {b: 2} calling copyProperties(objA, objB) 
will pollute objA and it will be equal to {a: 1, b: 2}.
*/

function copyProperties(objA, objB) {
  let objCopy = Object.assign(objA, objB);
  return objCopy;
}