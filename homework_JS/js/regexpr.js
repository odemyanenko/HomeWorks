/**
 Basics of validation with javascript
Non-RegExp Validation examples.
if(value) - value is not empty string, 0, undefined, null, NaN or false.
JavaScript Equality table

if(typeof value !== "number") - value is not a number. typeof - returns string name of the value.
typeof

if(value.lenght >= 0 && value.lenght <= 10) - value lenght property is from 0 to 10. Array and objects that can be processed like arrays (for example strings) have this property. In case of string it will be equal to the number of characters in the string.

if( new Date() instanceof Date ) - will return true if object to the left is instance of the object to the right. Can be used to check object constructor.

if("abc".charAt(0) === "a") - method charAt returns character at specific position of the string. Starts from the 0.

if("abc".indexOf("d") == -1) - method indexOf checks if string have a provided substring. If string don't have a provided substring it will return -1.

if(Number.isNaN(NaN)) - will return true. Number.isNaN checks if provided value is NaN. ES6 only.

if(Number.isInteger(123)) - will return true. Number.isInteger checks if provided value is an integer. ES6 only.

RegExp Validation examples.
Regular Expressions in JavaScript

regexp = /^[a-z]+$/; - example of regular expression in Javascript.

regexp.test(str) - method test will return true or false depending on if provided string matches the regular expression.

/^[0-9a-fA-F]+$/ - is hex number.

/^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$/g - is text in latin alphabet.

/^[a-z0-9_\-]+$/i - typical user name consisting from latin letters, numbers and symbols "_" and "-".

/^[0-9]{5}(?:-[0-9]{4})?$/ - a valid USA ZIP-code.

/^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/ - valid site URL.

/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ - a valid email.
 */
//-----------------------------------------------
//Is Email?
/*Given a string - check if it is a valid email. Return true if the email is valid, 
otherwise - return false. You need to use regular expressions.*/
function isValidEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

/*
let res;
res = isValidEmail('asdi@i.ua');
alert(res);
res = isValidEmail('asdi.i.ua');
alert(res);
*/
//-----------------------------------------------
//Is Latin?
/*Write a function that will return true if provided string consists only of the basic Latin letters and false if not.
For example, isLatin("abc") must return true. isLatin("ABC") must also return true. But isLatin("ab23c") must return false.*/

function isLatin(str) {
  var re = new RegExp('^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$', "g");
  //var re = new RegExp('[A-z]+', "g");
  return re.test(str);
}
/*
alert(isLatin("abc"));
alert(isLatin("ABC"));
alert(isLatin("ab23c"));*/
//-----------------------------------------------
//Normalize text
/**Write a function that will receive a string and will return it after normalizing it. 
 * To normalize text: remove space before and after the text (if any), 
 * replace two and more spaces to just one space. Doing so via regexp is preferred.
 * For example, normalizeText(" hello world ") will return string hello world.
 */

function normalizeText(text) {
  text = text.replace(/^\s+|\s+$/g, '');
  text = text.replace(/\s+/g, ' ');
  return text;
}
/*
console.log(normalizeText(" hello    wo   rld "));
alert(normalizeText(" hello wo  rl       d "));
*/
//-----------------------------------------------
//Text limitation
/*Write a function that will check a provided string. If string exceeds the limit of 100 
characters or 10 words - return true. Return false otherwise.
For example, isTextLimit("hello world") will return boolean false.*/

function isTextLimit(text) {
  let xWords = text.replace(/\S+/g, "x");
  let noWords = xWords.replace(/x/g, "");
  //  console.log(text.match(/\w\w+/g));  
  //  console.log(" Words: " + (xWords.length - noWords.length));  
  //  console.log(" Characters: " + (text.length - noWords.length));
  return (xWords.length - noWords.length > 10 || text.length - noWords.length > 100);
}
/*
alert(isTextLimit("hello world"));
alert(isTextLimit("hello wor ld wefdwefwe  wef wef  wef wef wef wef wef we wefwefwef we fwef wefw efwe fwef wef wef wef"));
*/

//-----------------------------------------------
//Is hexadecimal?
/*
 * Check if provided to a function string is a valid hexadecimal number. 
 * If it valid - return true. Return false otherwise.
 * For example, isHexadecimal("55ff00") will return boolean true.
 */
function isHexadecimal(str) {
  let re = /^[0-9A-F]+$/i;
  return re.test(str);
}
/*
alert(isHexadecimal("55ff00"));
alert(isHexadecimal("5sdv  ````` sdsdfd fsvsvsvd sdv5ff00"));*/

//-----------------------------------------------
//Character
/*Write a function that will return true if provided value is a character. Return false otherwise. 
A character is a string consisting of only one symbol.
For example, isChar("a") will return boolean true.
 */

function isChar(value) {
  let re = /^([0-9a-z]){1}$/i;
  //return (String(value).length === 1 && re.test(value));
  return (typeof value === "string" && re.test(value));
}
/*
alert(isChar("a"));
alert(isChar("ascsdcd"));
alert(isChar("1"));
alert(isChar(1));*/

//Guarantee Integer
/*You need to create a function that will guarantee that provided value will be an integer. 
If provided a number - function must return its natural equivalent (so a number like 2.5 
  must be returned as number 2). If provided a string that contains a number - cast to a natural number. 
  If provided boolean true or an object - return 1. For anything else return - 0.
For example, guaranteeInteger(true) will return number 1. guaranteeInteger({a: "foo"}) will return 1. 
guaranteeInteger(undefined) will return 0.*/

function guaranteeInteger(num) {
 // let re = /^[+-]?\d+(\.\d+)?$/;
  let numValue = Number(num);
  if (!Number.isNaN(numValue)) {
    return Math.round(numValue);
  }
  else {
    if (typeof num === "object") {return 1;}
    else {return 0;}
  }
}
/*
alert(guaranteeInteger(true));// will return number 1.
alert(guaranteeInteger({ a: "foo" }));// will return number 1.
alert(guaranteeInteger(undefined));// will return number 0.
*/

//Validate username
/*Check if provided to the function username is valid. Return true for valid and 
false if not. A valid name must consist of alphabet letters, numerals and 
symbols _ and -. All other symbols (including space) are not allowed in a valid name.
For example, isValidName("javascript_student-2018") will return boolean true.
 */
function isValidName(str) {
  let re = /^([0-9a-zа-я\-\_]+)$/i;
  return re.test(str);
}
/*
alert(isValidName("javascript_student-2018"));
alert(isValidName("javascript_s c c c ctudent-2018"));
alert(isValidName("javascript_~!#ent-2018"));
alert(isValidName("olga_demyanenko"));*/

//ZIP code
/*As part of the billing process for a USA-oriented store, you need to write a 
function that will check if the provided string value is valid USA ZIP code. 
A valid code consists of five digits or five digits and another four digits 
separated by a hyphen. Examples of ZIP codes: 12345, 12345-1234. 
Return true for valid ZIP codes and false for non-valid ones. RegExp preferred.

For example, isZipCodeUSA("45632-5692") will return boolean true.*/
function isZipCodeUSA(str) {
  let zipCodePattern = /^\d{5}$|^\d{5}-\d{4}$/;
  return zipCodePattern.test(str);
}
//alert(isZipCodeUSA("45632-5692"));

//Validate URL
/*Write a function that will check that provided value is a valid URL. 
Return true for valid and false for non-valid URLs. Use RegExp.
For example, isURL("http://www.example.com") will return boolean true.
 */
function isURL(str){
  let re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
  return re.test(str);
}

//alert(isURL("http://www.example.com"));