/*Write a function that will receive two arguments and will return the result of a concatenation of their string value.
For example, concate("ab", "c") should return "abc", concate(2, 2) should return "22".*/

function concateString(val1, val2){
  return String(val1).concat(val2);
}

/*
let val1 = prompt('Enter value for concat', "");
let val2 = 23.44;

let val3 = concateString(2, 2);
alert(val3);

val3 = concateString("ab", "c")
alert(val3);
*/
///////////////////////////////////////////////////////////
/*Create a function that will receive a string A and a string B. Return true if string A contains in it string B, or false if not. 
Make comparison non-case sensitive.
For example, contains("Hello World!", "world) should return true.*/

function contains(str, subString) {
  return String(str).search(new RegExp(subString, "i")) >= 0;
}
/*
let val3 = contains("ab", "ABC");
alert(val3);

val3 = contains("Hello World!", "world");
alert(val3);
*/
///////////////////////////////////////////////////////////
//Find the index of the substring
/*Write a function that will receive two strings - string A and string B. Find the index of string B position inside string A. 
If string A doesn't contain string B - return false.
For example, findIndex("abc", "bc") should return 1.*/

function findIndex(str, subString) {
  let res = String(str).indexOf(subString);
  if (res >= 0) { return res; }
  else { return false; }  
}
/*
let res ;

res = findIndex("abc", "bc");
alert(res);
res = findIndex("abc", "jnjnkn");
alert(res);

res = findIndex("", -34.55);
alert(res);*/

///////////////////////////////////////////////////////////
/*Write a function that will receive a string and a character as arguments. Search string for all occurrences of provided 
character and capitalize it.
For example, showCharacter("abc", "b") should return "aBc".*/

function showCharacter(str, chr) {
  let varStr = String(str);
  let resStr = "";

  for (let i = 0; i < varStr.length; i++) {
    if (varStr[i] === chr) {
      resStr += varStr[i].toUpperCase();
    }
    else { resStr += varStr[i]; }
  }
  return resStr;
}

function showCharacter2(str, chr) {
  let varStr = String(str);
  let replaceChr = String(chr).toUpperCase();

  varStr = varStr.replace(new RegExp(String(chr), 'g'), replaceChr);

  return varStr;
}

/*
let res = showCharacter2("abc sf fsdf sdb sdfsd sd b gsdfbbbb sdfsdf s", "f");
alert(res);
*/

///////////////////////////////////////////////////////////
//Short name
/*Write a function that will receive a name as an argument and will return its short form. So a name like John Adams will be 
formatted as John A.
For example, abbrName("Richard Mabey") will return Richard M.*/

function abbrName(str) {
  let varStr = String(str);
  let findIndex = varStr.indexOf(" ");

  if (findIndex >= 0) {
    if (findIndex + 1 < varStr.length) {
      varStr = varStr.substring(0, findIndex + 1) + varStr.substring(findIndex + 1, findIndex + 2) + ".";
    }
    else { 
      varStr = varStr.substring(0, findIndex); 
    }
  }
  return varStr;
}
/*
let res;

res = abbrName("Richard Mabey");
alert(res);

res = abbrName("Richard");
alert(res);

res = abbrName("Richard ");
alert(res);

res = abbrName("John Adams");
alert(res);*/
///////////////////////////////////////////////////////////
//Capitalize words
/*Write a function that will receive a string and will return it with all words being capitalized. So a hello world will be 
returned as Hello World.
For example, capitalizeWords("capitalize words") will return Capitalize Words.*/
function capitalizeWords(str) {
  let varStr = String(str);

  varStr = varStr.replace(/\b\w/g, varLetter => varLetter.toUpperCase());
  //varStr = varStr.replace(/\b\w/g, function(varLetter){ return varLetter.toUpperCase() })
  //varStr = varStr.replace(/\b\w/g, varLetter => varLetter.charAt(0).toUpperCase() + varLetter.slice(1));
 return varStr;
}
/*
let res;

res = capitalizeWords("capitalize words wfewef wef wefwef");
alert(res);

res = capitalizeWords("hello world");
alert(res);*/

///////////////////////////////////////////////////////////
//camelCase
/*Write a function that will camelize provided string. So string "hello world" will become "helloWorld". 
The first letter must remain in lower case.
For example, camelCase("java script") will return javaScript.*/
function camelCase(str) {  
  let resStr = "";    
  let words = String(str).split(" ");
  for (let i = 0; i < words.length; i++){
    if (i === 0){
      resStr += words[i].toLocaleLowerCase();
    }
    else resStr += words[i].replace(/\b\w/g, varLetter => varLetter.toLocaleUpperCase());
  }
  return resStr;
}

/*
let res;
res = camelCase("hello world fsd sdf sd  sd fsdf sd sdf sdfsdfs sdf sdf");
alert(res);
res = camelCase("DFSFFDF java script");
alert(res);
*/

///////////////////////////////////////////////////////////
//Is Upper Case?
/*Write a function that will return true if the provided string starts from the upper case letter and false if not.
For example, isUpperCase("JavaScript") will return true and isUpperCase("javaScript") will return false.*/
function isUpperCase(str) {
  let varString = String(str);
  return (varString.length > 0) && (varString.charAt(0) === varString.charAt(0).toLocaleUpperCase());
}

/*
let res;
res = isUpperCase("JavaScript");
alert(res);
res = isUpperCase("javaScript");
alert(res);
*/

///////////////////////////////////////////////////////////
//Make title
/*Write a function that will return provided string in a title case. So a string "hello WORLD" will become "Hello World". 
Make all letters except first - in lower case. Make the first letter - in upper case.
For example, titleCase("hello javascript") will return Hello Javascript.*/
function titleCase(str) {
  let resStr = "";    
  let words = String(str).toLocaleLowerCase().split(" ");
  
  for (let i = 0; i < words.length; i++){
    resStr += words[i].replace(/\b\w/g, varLetter => varLetter.toLocaleUpperCase());
    if (resStr.length < String(str).length) {
      resStr += " ";
    }
  }
  
  return resStr;
}

let res;
res = titleCase("hello javascript");
alert(res);
res = titleCase("hello WORLD");
alert(res);