/*
Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с 
помощью модальных окон браузера - alert, prompt, confirm.

Технические требования:

Считать с помощью модельного окна браузера данные пользователя: имя и возраст. 

Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.

Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: 
Are you sure you want to continue? и кнопками Ok, Cancel. 
Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя. 
Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.

Если возраст больше 22 лет - показать на экране сообщение: Welcome, + имя пользователя.
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

Не обязательное задание продвинутой сложности:
После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе 
возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
*/

const USERNAME_TXT = 'Enter Your UserName, please...';
const USERAGE_TXT = 'Enter Your Age (whole years > 0), please...';

const CONTINUE_TXT = 'Are you sure you want to continue?';
const WELCOME_TXT = 'Welcome, ';
const NOTALLOWED_TXT = 'You are not allowed to visit this website.';

function validateUserName() {
  let value = '';
  while (!value) {
    value = prompt(USERNAME_TXT, value);
    if (value === null) { value = ''; }
  }
  return value;
}

function validateUserAge() {
  let value = '';
  while (!value || Number.isInteger(Number(value)) != true || Number(value) <= 0) {
    value = prompt(USERAGE_TXT, value);
    if (value === null) { value = ''; }
  }
  return Number(value);
}

let userName = validateUserName();
let userAge = validateUserAge();

switch (true) {
  case (userAge < 18):
    alert(NOTALLOWED_TXT);
    break;
  case (userAge >= 18 && userAge <= 22):
    if (confirm(CONTINUE_TXT)) {
      alert(WELCOME_TXT + userName)
    }
    else alert(NOTALLOWED_TXT);
    break;
  default: alert(WELCOME_TXT + userName);
}