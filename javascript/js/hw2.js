/*
Реализовать программу на Javascript, которая будет находить 
все простые числа в заданном диапазоне.

Технические требования:

Считать с помощью модального окна браузера число, которое введет пользователь. 
Вывести в консоли все простые числа от 1 до введенного пользователем числа.
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

Не обязательное задание продвинутой сложности:

Проверить, что введенное значение является целым числом, и больше единицы. 
Если данные условия не соблюдаются, повторять вывод окна на экран до тех пор, 
пока не будет введено целое число больше 1.
Максимально оптимизировать алгоритм, чтобы он выполнялся как можно быстрее для больших чисел.
Считать два числа, m и n. Вывести в консоль все простые числа в диапазоне от m до n 
(меньшее из введенных чисел будет m, бОльшее будет n). 
Если хотя бы одно из чисел не соблюдает условия валидации, указанные выше, 
вывести сообщение об ошибке, и спросить оба числа заново.
 */

const CONFIRMRANGE_TXT = 'Calculate number in range?';
const RANGENUMBER_TXT = 'Enter number ( > 1), please...';
const ERROR_TXT = 'Invalidate value! ';
const MINRANGE = 1;

function getNumber(valMinRange) {
  let value, count = 0;
  while (!value || Number.isInteger(Number(value)) != true || Number(value) <= valMinRange) {
    if (count > 0) {
      value = prompt(ERROR_TXT + RANGENUMBER_TXT, value);
    }
    else {
      value = prompt(RANGENUMBER_TXT, value);
    }
    if (value === null) { value = ''; }
    count++;
  }
  return Number(value);
}

function isPrime(value) {
  if (value < 2) return false;

  let q = Math.floor(Math.sqrt(value));
  for (let i = 2; i <= q; i++) {
    if (value % i === 0) {
      return false;
    }
  }

  return true;
}

let isRangeNumbers = confirm(CONFIRMRANGE_TXT);
let startRange;

if (isRangeNumbers === true) {
  startRange = getNumber(MINRANGE);
}
else startRange = MINRANGE;

let finishRange = getNumber(startRange);

for (let i = startRange; i <= finishRange; i++) {
  if (isPrime(i)) {
    console.log(i);
  }
}



