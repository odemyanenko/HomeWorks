/*
Реализовать функцию подсчета факториала числа.

Технические требования:

Написать функцию подсчета факториала числа.
Считать с помощью модального окна браузера число, которое введет пользователь.
С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
Использовать синтаксис ES6 для работы с перемеными и функциями. 
 */

const MESSAGE_TXT = 'Enter Number for calculate factorial, please...';
const ERROR_TXT = 'Invalidate value!';

function calcFactorial(value) {
  if (value === 0) {
    return 1;
  }
  else {
    return value * calcFactorial(value - 1);
  }
}

let num = prompt(MESSAGE_TXT, '');

if (!num || num.trim() === "" || Number.isInteger(Number(num)) != true || Number(num) < 0) {
  alert(ERROR_TXT);
}
else {
  let result = calcFactorial(Number(num));
  alert(num +'! = ' + result);  
}