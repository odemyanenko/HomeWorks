/*
Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.

Технические требования:

Написать функцию для подсчета n-го обобщенного числа Фибоначчи. 
Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности 
(могут быть любыми целыми числами), 
n - порядковый номер числа Фибоначчи, которое надо найти. 
Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.

Считать с помощью модального окна браузера число, которое введет пользователь (n).
С помощью функции посчитать n-е число в обощенной последовательности Фибоначчи и вывести его на экран.
Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
Использовать синтаксис ES6 для работы с перемеными и функциями.
*/

const INPUT1_TXT = 'Enter 1 number list, please...';
const INPUT2_TXT = 'Enter 2 number list, please...';
const INPUTN_TXT = 'Enter length list, please...';
const ERROR_TXT = 'Invalidate value!';

function getNumber(promptStr) {
  let value, count = 0;
  while (!value || value.trim() === "" || Number.isInteger(Number(value)) != true) {
    if (count > 0) {
      value = prompt(ERROR_TXT + ' ' + promptStr, value);
    }
    else {
      value = prompt(promptStr, value);
    }
    if (value === null) { value = ''; }
    count++;
  }
  return Number(value);
}

function calcFibonacci(num) {
  if (num === 0) {
    return f0;
  }
  else if (num === 1) {
    return f1;
  }
  else if (n > 2 && num >= 2) {
    return calcFibonacci(num - 2) + calcFibonacci(num - 1);
  }
  else if (n < 0 && num < 0){
    return calcFibonacci(num + 2) - calcFibonacci(num + 1);
  }
}

let f0 = getNumber(INPUT1_TXT);
let f1 = getNumber(INPUT2_TXT);
let n = getNumber(INPUTN_TXT);

let valFibonacci = calcFibonacci(n);
alert ('Result = ' + valFibonacci);
