/*
Реализовать функцию полного клонирования объекта.

Технические требования:

Написать функцию для рекурсивного полного клонирования объекта 
(без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности. 
Использовать синтаксис ES6 для работы с перемеными, функциями и объектами.
 */

function cloneObject(obj) {
  let clone;
  clone = Array.isArray(obj) ? []: {};
  for (let i in obj) {
    if (typeof (obj[i]) == "object" && obj[i] != null) {
      clone[i] = cloneObject(obj[i]);
    }
    else { clone[i] = obj[i]; }
  }
  return clone;
}

let objBase = {
  id: 1, name: 'Petrov', sex: 'M', birthdate: '1987-06-23',
  documents: [
    { docid: 1, type: 'passport', series: 'WR', number: '234567' },
    { docid: 2, type: 'drivelicence', series: 'RT', number: '086533' },
  ],
  address: [
    { addrid: 1, type: 'residance', address: '08765, Ukraine', addinfo: [9, 7, 6, 4] },
    { addrid: 2, type: 'birth', address: '08265, Ukraine', addinfo: [1, 2, 3] }
  ]
};

let objNew = cloneObject(objBase);

objNew.address[0].addinfo[1] = 99;//check changes only in new Object

console.log(objBase);
console.log(objNew);


