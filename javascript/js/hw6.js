/*
 Реализовать функцию-конструктор для создания объекта "пользователь".

Технические требования:

Написать функцию createNewUser(), которая будет создавать и возвращать объект "пользователь".
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект со свойствами firstName и lastName.
Добавить в объект метод getLogin(), который будет возвращать первую букву имени пользователя, 
соединенную с фамилией пользователя, все в нижнем регистре.

Не обязательное задание продвинутой сложности:
Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. 
Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свйоства.
 */

function createNewUser(){
  const FIRSTNAME_TXT = 'Enter User First Name, please...';
  const LASTNAME_TXT = 'Enter User Last Name, please...';
  const ERROR_TXT = 'Invalidate value!';

  let firstName = '';
  let lastName = '';

  let count = 0;
  while (!firstName || firstName.trim() === '') {
    if (count > 0) { firstName = prompt(ERROR_TXT + ' ' + FIRSTNAME_TXT, firstName); }
    else { firstName = prompt(FIRSTNAME_TXT); }
    if (firstName === null) { firstName = ''; }
    count++;
  };

  count = 0;
  while (!lastName || lastName.trim() === '') {
    if (count > 0) { lastName = prompt(ERROR_TXT + ' ' + LASTNAME_TXT, lastName); }
    else { lastName = prompt(LASTNAME_TXT); }
    if (lastName === null) { lastName = ''; }
    count++;
  };

  this.setFirstName = function (value) {
    firstName = value;
  }
  this.getFirstName = function () {
    return firstName;
  };

  this.setLastName = function (value) {
    lastName = value;
  }
  this.getLastName = function () {
    return lastName;
  };

  this.getLogin = function () {
    return firstName.toLowerCase().substr(0, 1) + lastName.toLowerCase();
  }
}

let user = new createNewUser();
//user.setFirstName('Vasya');
//user.setLastName('Petrov');

alert('Login for ' + user.getFirstName() + ' ' + user.getLastName() + ' is: ' + user.getLogin());
