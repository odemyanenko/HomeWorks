/**
 Реализовать функцию нахождения уникальных объектов в массиве 
 при сравнении с другим массивом, по указанному пользователем свойству.

Технические требования:

Написать функцию excludeBy(), которая в качестве первых двух параметров будет 
принимать на вход два массива с объектами любого типа, например:
const users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}]

В качестве третьего параметра функция должна принимать имя поля, 
по которому будет проводиться сравнение.

Функция должна вернуть новый массив, который будет включать те объекты 
из первого массива, значение указанного свойства которых не встречается 
среди объектов, представленных во втором массиве. Например, 
вызов функции excludeBy(peopleList, excluded, 'name') должен возвращать 
массив из тех пользователей peopleList, имя которых (свойство name) 
не встречается у пользователей, которые находятся в массиве excluded.
 */

function excludeBy(sourceArray, excludedArray, prop) {
  let unicArray = [];

  unicArray = sourceArray.filter(function (obj) {
    return excludedArray.some(elemValue => elemValue[prop] != obj[prop]);
  });

  return unicArray;
}

let baseUsers = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
  name: "Anna",
  surname: "Ivanova",
  gender: "female",
  age: 22
}];

let restrictedUsers = [{
  name: "Anna",
  surname: "Ivanova",
  gender: "female",
  age: 22
}];

let resultUsers = excludeBy(baseUsers, restrictedUsers, 'surname');
console.log(resultUsers);