/*
Реализовать функцию, которая будет считать возраст пользователя и его знак зодиака.

Технические требования:

При запуске программы - в диалоговом окне спросить дату рождения пользователя (текст в формате dd.mm.yyyy)
Вывести в модальном окне сообщение: Вам ? лет! - Вместо ? подставить возраст пользователя.
Вывести в отдельном модальном окне знак зодиака пользователя в формате: Ваш знак зодиака: ?.


Не обязательное задание продвинутой сложности:

Выводить также в год какого животного по китайскому календарю родился пользователь.
*/

const BIRTHDATE_TXT = 'Enter Your BirthDate (dd.mm.yyyy), please...';

function calculateAge(par) {
  let diff_ms = Date.now() - par.getTime();
  let age_dt = new Date(diff_ms);

  return Math.abs(age_dt.getUTCFullYear() - 1970);
}

function getZodiac_Sign(day, month) {
  const ZODIACLIST_SIGN = [
    { nameEN: 'Capricorn', nameRU: 'Козерог', sDay: 22, sMonth: 12, fDay: 20, fMonth: 1 },
    { nameEN: 'Aquarius', nameRU: 'Водолей', sDay: 21, sMonth: 1, fDay: 18, fMonth: 2 },
    { nameEN: 'Pisces', nameRU: 'Рыбы', sDay: 19, sMonth: 2, fDay: 20, fMonth: 3 },
    { nameEN: 'Aries', nameRU: 'Овен', sDay: 21, sMonth: 3, fDay: 20, fMonth: 4 },
    { nameEN: 'Taurus', nameRU: 'Телец', sDay: 21, sMonth: 4, fDay: 20, fMonth: 5 },
    { nameEN: 'Gemini', nameRU: 'Близнецы', sDay: 21, sMonth: 5, fDay: 20, fMonth: 6 },
    { nameEN: 'Cancer', nameRU: 'Рак', sDay: 21, sMonth: 6, fDay: 22, fMonth: 7 },
    { nameEN: 'Leo', nameRU: 'Лев', sDay: 23, sMonth: 7, fDay: 22, fMonth: 8 },
    { nameEN: 'Virgo', nameRU: 'Девы', sDay: 23, sMonth: 8, fDay: 22, fMonth: 9 },
    { nameEN: 'Libra', nameRU: 'Весы', sDay: 23, sMonth: 9, fDay: 22, fMonth: 10 },
    { nameEN: 'Scorpio', nameRU: 'Скорпион', sDay: 23, sMonth: 10, fDay: 21, fMonth: 11 },
    { nameEN: 'Sagittarius', nameRU: 'Стрелец', sDay: 22, sMonth: 11, fDay: 21, fMonth: 12 }
  ];
  
  //console.log(ZODIACLIST_SIGN.filter(item => ((item['sMonth'] === month && day >= item['sDay']) || (item['fMonth'] === month && day <= item['fDay']))));
  return ZODIACLIST_SIGN.filter(item => ((item['sMonth'] === month && day >= item['sDay']) || (item['fMonth'] === month && day <= item['fDay'])))[0];
}

function getZodiac_Chinese(year) {
  const ZODIACLIST_CHINESE = ["Monkey", "Cock", "Dog", "Boar", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep"];

  return ZODIACLIST_CHINESE[(year % ZODIACLIST_CHINESE.length)];
}

let re = /^\d{1,2}\.\d{1,2}\.\d{4}$/;
let minYear = 1902;
let maxYear = (new Date()).getFullYear();

let dateBirth = prompt(BIRTHDATE_TXT, '');

if (dateBirth != '' && !dateBirth.match(re)) {
  alert("Invalid date format: " + dateBirth);
}
else {
  let parts = dateBirth.split('.');

  if (parts[0] < 1 || parts[0] > 31) {
    alert('Invalid value for day: ' + parts[0]);
  } else if (parts[1] < 1 || parts[1] > 12) {
    alert('Invalid value for month: ' + parts[1]);
  } else if (parts[2] < minYear || parts[2] > maxYear) {
    alert('Invalid value for year: ' + parts[2] + ' - must be between ' + minYear + ' and ' + maxYear);
  }
  else {
    let currDate = new Date(parts[2], parts[1] - 1, parts[0]);

    let day = currDate.getDate();
    let month = currDate.getMonth() + 1;
    let year = currDate.getFullYear();

    let objZodiacSign = getZodiac_Sign(day, month);

    alert('Вам ' + calculateAge(currDate).toString() + ' лет!');
    alert('Ваш знак зодиака: ' + objZodiacSign['nameEN'] + ' (' + objZodiacSign['nameRU'] + ').');
    alert('Ваш знак по китайскому календарю: ' + getZodiac_Chinese(year) + ').');
  }
}